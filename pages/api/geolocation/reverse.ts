import type { NextApiRequest, NextApiResponse } from 'next'
import { OW_API_KEY } from '../../../configs';

const BASE_URL = `https://api.openweathermap.org/geo/1.0/reverse?appid=${OW_API_KEY}&limit=10`;

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<IGeolocationData[]>
) {
    const { query: {lat, lon} } = req;

    const response = await (await fetch(`${BASE_URL}&lat=${lat}&lon=${lon}`)).json();
    
    res.status(200).json(response)
}

