# KI Challenge

Hello!
Here is my project to the tech challenge.

Before proceeding to instructions, here are some notes:

I opted to keep it as simple and minimal as possible, avoiding use any kind of library or frameworks, using only Next, React and Typescript as it is the tech stak that you guys use internally. 

I don't have much expertise with testing frameworks, so I leave test out of the scope of this project.

# Requirements checklist 

### Part 1
- [x] React
- [x] Current location (Leiria fallback)
- [x] Current weather
- [x] Forecast next 7 days
- [x] Weather and Forecast images

### Part 2
- [x] Save city
- [x] Remove city
- [x] Weather and forecast for favorites
- [x] Persist favorites list
- [x] Give feedback 

# Running the project

## Docker

`docker build -t gk-challenge-docker .`

`docker run -p 3000:3000 gk-challenge-docker`

Open your browser (preferable chrome) on localhost:3000

## Local

`yarn && yarn build`

`yarn start`

Open your browser (preferable chrome) on localhost:3000
