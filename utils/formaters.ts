

//Formats temperature to string without decimals and with ºC
export function formatTemperatureToString(temperature: number, removeUnitSymbol= false): string {
    let tempString = temperature.toString();

    if (tempString.indexOf('.') > -1) {
        tempString = tempString.substring(0, (tempString.indexOf('.')));
    }

    return `${tempString}${removeUnitSymbol? 'º': ' ºC' }`;
}


//Format location name for IGeolocationData
export function formatLocationName(loc: IGeolocationData) {
    return `${loc.name} - ${loc.state ? loc.state + ' -' : ''} ${loc.country}`;
}