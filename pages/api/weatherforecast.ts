import type { NextApiRequest, NextApiResponse } from 'next'
import { OW_API_KEY, UNITS } from '../../configs';

const BASE_URL = `https://api.openweathermap.org/data/2.5/onecall?appid=${OW_API_KEY}&exclude=hourly,minutely,alerts&units=${UNITS}`;

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<IWeatherForecastData>
) {
    const { query: { lat, long } } = req;

    const response = await (await fetch(`${BASE_URL}&lat=${lat}&lon=${long}`)).json();
    
    res.status(200).json(response)
}

