
interface IWeather {
  id: number;
  main: string;
  description: string;
  icon: string;
}

interface IWeatherDetails {
  temp: number;
  temp_min: number;
  temp_max: number;
}

interface ICoordinations {
  lon: number;
  lat: number;
}

interface IWeatherData {
  coord: ICoordinations;
  weather: IWeather[];
  base: any;
  main: IWeatherDetails;
  visibility: number;
  dt: number;
  sys: {
    type: number;
    id: number;
    message?: number;
    country: string;
    sunrise: number;
    sunset: number;
  };
  timezone: number;
  id: number;
  name: string
  cod: number;
}

interface IWeatherForecastData {
  lat: number;
  lon: number;
  timezone: string;
  timezone_offset: number;
  current: ICurrentWeather;
  daily: IDayWeather[];
  location?: IGeolocationData;
}

interface IDayWeather {
  dt: number;
  sunrise: number;
  sunset: number;
  temp: {
    day: number;
    min: number;
    max: number;
  };
  weather: IWeather[];
}

interface ICurrentWeather {
  dt: number;
  sunrise: number;
  sunset: number;
  temp:  number;
  weather: IWeather[];
}

interface IGeolocationData {
  name: string;
  lat: number;
  lon: number;
  country: string;
  state?: string;
  local_names: { [key: string]: [name: string] };
}

interface ISavedLocation {
  name: string;
  coord: ICoordinations;
}


//Interface that remove the obligation to mandatory fields not used in the GeolocationPosition model
interface OptionalGeolocationPosition {
  coords: {
    latitude: number,
    longitude: number
  }
}

interface IAlert {
    alertType: 'info' | 'error';
    message: string;
}