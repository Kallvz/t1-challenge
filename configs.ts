
//API KEY OppenWeather
export const OW_API_KEY = '70b13913945899d93ff4da7caf45738f';

// Mesure Units
export const UNITS = 'metric'; // 'standard' // 'imperial'


//Base URLs
export const API_URL = '/api';
export const ICON_URL = 'http://openweathermap.org/img/wn/{ID}@4x.png';


//LocalStorage Keys
export const LOCATION_LIST_STORAGE_KEY = 'wthrLocationList';


//Default position options
export const POSITION_OPTIONS: PositionOptions = {
    maximumAge: 1000 * 60 * 30,
    enableHighAccuracy: true,
    timeout: 1000 * 5
}

//Default position (LEIRIA)
export const DEFAULT_POSITION: GeolocationPosition = {
    coords: {
        latitude: 39.74362,
        longitude: -8.80705,
        accuracy: 0,
        altitude: 0,
        altitudeAccuracy: 0,
        heading: 0,
        speed: 0,
    },
    timestamp: new Date().getTime()
}

//Default Alert Timeout
export const DEFAULT_ALERT_TIMEOUT = 1000 * 2.5;