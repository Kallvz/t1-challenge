/**
 *  Weather Service
 * 
 *  Exposes functions to fetch and transform data related with the Weather API.
 */

import { API_URL } from "../configs";
import GeolocationService from "./GeolocationService";

const SERVICE_PATH = '/weather'

// Get current weather
async function getCurrentWeather(position: GeolocationPosition | OptionalGeolocationPosition ): Promise<IWeatherData> {

    if (!position) return Promise.reject('Position must be provided!');

    let url = API_URL + SERVICE_PATH;

    try {
        
        const { latitude, longitude } = position.coords;
    
        return fetch(`${url}?lat=${latitude}&long=${longitude}`).then(resp => resp.json());
        
    } catch (error) {
        throw error
    }
}

// Get weather and forecast for next 7 days
async function getWeatherAndForecast(position :GeolocationPosition | OptionalGeolocationPosition ): Promise<IWeatherForecastData> {

    if (!position) return Promise.reject('Position must be provided!');

    let url = API_URL + '/weatherforecast';

    try {
        
        const { latitude, longitude } = position.coords;
    
        return fetch(`${url}?lat=${latitude}&long=${longitude}`).then(async resp => {
            
            const data: IWeatherForecastData = await resp.json();

            const geoData: IGeolocationData[] = await GeolocationService.getLocationNameByPosition({ latitude: data.lat, longitude: data.lon });

            if(geoData.length > 0) data.location = geoData[0]; 

            return Promise.resolve(data)
        });
        
    } catch (error) {
        throw error
    }
}

export default {
    getCurrentWeather,
    getWeatherAndForecast
}