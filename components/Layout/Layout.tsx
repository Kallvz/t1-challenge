import React, { ReactNode } from "react";
import Head from "next/head";

import styles from "./styles.module.css"

export interface ILayoutProps {
  children: ReactNode
}

const Layout: React.FC<ILayoutProps> = ({children}) => {
    
    
    return (
        <>
          <Head>
            <title>Kalls Weather App</title>
            <meta name="description" content="T1 Challenge" />
            <link rel="icon" href="/favicon.ico" />
          </Head>

          <main className={styles.main}>
            {children}
          </main>

          <footer className={styles.footer}>
            Powered by Guilherme Kalel
          </footer>
      </>
    )
}

export default Layout;