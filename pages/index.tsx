import type { NextPage } from 'next'
import LocationsList from '../components/LocationsList'
import WeatherDetail from '../components/WeatherDetail'
import AppContextProvider from '../contexts/AppContext'
import styles from '../styles/Home.module.css'

const Home: NextPage = () => {

  return (
    <div className={styles.container} >
      <AppContextProvider >

        <WeatherDetail />

        <LocationsList />

      </AppContextProvider>
    </div>
  )
}

export default Home
