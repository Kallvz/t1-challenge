
import Image from "next/image"
import { ICON_URL } from "../../configs";
import { formatTemperatureToString } from "../../utils/formaters";

import styles from "./styles.module.css"

export interface IForecastCardProps {
    forecast: IDayWeather;
}

const ForecastCard: React.FC<IForecastCardProps> = ({ forecast }) => {

    const { dt, temp, weather} = forecast;

    const day = new Date(dt * 1000);

    return (
        <div className={styles.card} >
            
            <h4>{day.getDate()}</h4>

            <Image src={ICON_URL.replace('{ID}', weather[0].icon)} width={80} height={80} alt="Forecast"/>
            
            <div className="column" >
                {formatTemperatureToString(temp.day)}

                <span className={styles.description}>
                    {weather[0].main}
                </span>
            </div>
            
            <div className={styles.maxMin}>
                <p className="no-margin">&#43; {formatTemperatureToString(temp.max, true)}</p>
                <p className="no-margin">&minus; {formatTemperatureToString(temp.min, true)}</p>
            </div>

        </div>
    )

}


export default ForecastCard;