import type { NextApiRequest, NextApiResponse } from 'next'
import { OW_API_KEY } from '../../../configs';

const BASE_URL = `https://api.openweathermap.org/geo/1.0/direct?appid=${OW_API_KEY}&limit=10`;

export default async function handler(
    req: NextApiRequest,
    res: NextApiResponse<IGeolocationData[]>
) {
    const { query } = req;

    const response = await (await fetch(`${BASE_URL}&q=${query.query}`)).json();
    
    res.status(200).json(response)
}

