import Link from "next/link";
import { useRouter } from "next/router";
import { ReactNode } from "react";

import styles from "./styles.module.css"

export interface ICardProps {
    title: string;
    link?: string;
    children?: ReactNode
}

const Card: React.FC<ICardProps> = ({title, link, children}) => {

    const router = useRouter();

    function handleClick() {
        if (link) router.push(link);
    }

    return (
        <div className={styles.card} onClick={handleClick}>
            {/* <h2>{title} &rarr;</h2> */}
            <h2>{title}</h2>
            {children}
        </div>
    )

}


export default Card;