import { ReactNode, useEffect } from "react";
import { useAppContextData } from "../../contexts/AppContext";

import styles from "./styles.module.css"


const AlertBox: React.FC = () => {

    const { alerts } = useAppContextData();

    if (!alerts || alerts.length < 1) return null;

    return (
        <div className={styles.alertWrapper}>
            { alerts && alerts.map(alert => (
                <div className={`${styles.alertBox} ${alert.alertType === 'error' && styles.error}`} key={Math.random().toString()} >
                    {<h1>{alert.message}</h1>}
                </div>
            ))}
        </div>
    )

}


export default AlertBox;