import { useEffect, useRef, useState } from "react";
import { LOCATION_LIST_STORAGE_KEY } from "../../configs";
import LocationCard from "../LocationCard";
import GeolocationService from "../../service/GeolocationService";
import { formatLocationName } from "../../utils/formaters";
import { useAppContextData } from "../../contexts/AppContext";
import usePosition from "../../hooks/usePosition";
import styles from "./styles.module.css"

export interface ILocationListProps {
}

const LocationList: React.FC<ILocationListProps> = () => {

    const inputQueryRef = useRef<HTMLInputElement>(null);
    const [savedLocations, setSavedocations] = useState<ISavedLocation[]>([]);
    const [fetchedLocations, setFetchedLocations] = useState<IGeolocationData[]>();
    const [showResultBox, setShowResultBox] = useState(false);
    const { setSelectedLocation, showAlert } = useAppContextData();
    const { position } = usePosition();

    useEffect(() => {
        //Loads saved locations from localStorage
        // if (typeof window !== "undefined") {
            const savedLocations = JSON.parse(localStorage.getItem(LOCATION_LIST_STORAGE_KEY) || '[]');
            if (savedLocations && savedLocations.length > 0) setSavedocations(savedLocations);
        // }
    }, []);

    //Handle add location to local favorites
    function handleAddLocation(location: IGeolocationData) {
        inputQueryRef.current!.value = ''
        setFetchedLocations([]);

        const newSavedLocation: ISavedLocation = {
            name: formatLocationName(location),
            coord: { lon: location.lon, lat: location.lat}
        }
        
        //Add new location to list
        const updatedSaveLocations: ISavedLocation[] = [...savedLocations, newSavedLocation]
        
        //Saves to localStorage
        localStorage.setItem(LOCATION_LIST_STORAGE_KEY, JSON.stringify(updatedSaveLocations));
        
        showAlert({ alertType: "info", message: `Added '${newSavedLocation.name}' to favorites list` });
        setSavedocations(updatedSaveLocations);
    }

    //Handle removing a location from stored favorites
    function handleRemoveLocation(location: ISavedLocation) {

        const {lat, lon} = location.coord

        //remove location from current list
        const updatedSaveLocations = savedLocations.filter(loc => !(loc.coord.lat == lat && loc.coord.lon == lon));

        //Save to localStorage
        localStorage.setItem(LOCATION_LIST_STORAGE_KEY, JSON.stringify(updatedSaveLocations));

        showAlert({ alertType: "info", message: `'${location.name}' removed from favorites list` });
        setSavedocations(updatedSaveLocations);
    }
    
    //Handle user input on location query
    async function handleQuery(locationQuery: string ) {
        if (!locationQuery || locationQuery.length < 4) return setFetchedLocations([])

        let list: IGeolocationData[] = await GeolocationService.cachedGetLocationByName(locationQuery).catch(() => []);

        if (!Array.isArray(list)) list = [];

        //Remove locations already added
        list = list.filter(loc => !savedLocations.find(sl => (sl.coord.lat == loc.lat && sl.coord.lon == loc.lon)));

        setFetchedLocations(list)
    }

    //Handle select a location from list
    function handleSelectLocation(location: ISavedLocation) {
        if (setSelectedLocation) {
            showAlert({ alertType: "info", message: `${location.name} selected!` });
            setSelectedLocation({
            coords: {
                latitude: location.coord.lat,
                longitude: location.coord.lon
            }
        })
        }
    }

    return (
        <div className={styles.main} >

            <h2 className={styles.title}>Locations</h2>

            <div className={styles.inputComponent} >
                <input
                    type="text"
                    ref={inputQueryRef}
                    className={styles.inputQuery}
                    onChange={e => handleQuery(e.target.value)}
                    placeholder="Type here to seach a location"
                    onFocus={() => setShowResultBox(true)}
                    onBlur={() => setTimeout(() => setShowResultBox(false), 100)}
                />

                {(showResultBox && fetchedLocations && (inputQueryRef.current?.value || '').length > 3) && (
                    <ul className={styles.queryResultBox} >
                        {(fetchedLocations.length == 0 && (inputQueryRef.current?.value || '').length > 3 ) && (
                            <span>Nothing found :( </span>
                        )}
                        {fetchedLocations.map((loc, i) => (
                            <li key={i} onClick={() => handleAddLocation(loc)}>
                                {formatLocationName(loc)}
                            </li>
                        ))}
                    </ul>   
                )}
            </div>
            
            {/* Add a card to Users current position */}
            {position && (() => {
                const loc = {name: 'My position',  coord:{lat: position.coords.latitude, lon: position.coords.longitude}}
                return (<LocationCard location={loc} key={'userPosition'} onClick={() => handleSelectLocation(loc)} />)
            })()}
            {savedLocations && savedLocations.map((loc, i) => (
                <LocationCard location={loc} key={i} removeLocationFn={handleRemoveLocation} onClick={() => handleSelectLocation(loc)} />
            ))}

        </div>
    )

}


export default LocationList;