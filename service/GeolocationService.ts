/**
 *  Geolocation Service
 * 
 *  Exposes functions for direct and reverse geocoding.
 */

import { API_URL } from "../configs";
import { CacheWrapper } from "../utils/functions";

const SERVICE_PATH = '/geolocation'

// Query location list by name
async function getLocationByName(query: string): Promise<IGeolocationData[]> {

    if (!query) return Promise.reject('Location name must be provided');

    let url = API_URL + '/geolocation/direct'

    try {
        
        return fetch(`${url}?query=${query}`).then(res => res.json())
        
    } catch (error) {
        throw error
    }
}

// Query location name by position
async function getLocationNameByPosition(position: {latitude: number, longitude: number}): Promise<IGeolocationData[]> {

    if (!position) return Promise.reject('Position must be provided');

    let url = API_URL + '/geolocation/reverse'

    try {
        
        const { latitude, longitude } = position;

        return fetch(`${url}?lat=${latitude}&lon=${longitude}`).then(res => res.json())
        
    } catch (error) {
        throw error
    }
}

const cachedGetLocationByName = CacheWrapper(getLocationByName);

export default {
    getLocationByName,
    getLocationNameByPosition,
    cachedGetLocationByName
}