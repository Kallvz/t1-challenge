import Image from "next/image";
import { MouseEventHandler, useEffect, useState } from "react";
import { ICON_URL } from "../../configs";
import WeatherService from "../../service/WeatherService";
import { formatTemperatureToString } from "../../utils/formaters";

import styles from "./styles.module.css"

export interface ILocationCardProps {
    location: ISavedLocation;
    removeLocationFn?: (loc: ISavedLocation) => void;
    onClick?: () => void;
}

const LocationCard: React.FC<ILocationCardProps> = ({location, removeLocationFn, onClick}) => {

    const [weather, setWeater] = useState<IWeatherData>();

    useEffect(() => {
        const { lat, lon } = location.coord;

        WeatherService.getCurrentWeather({ coords: { latitude: lat, longitude: lon } }).then(resp => {
            setWeater(resp);
        })

    },[location.coord]);

    function handleRemoveClick() {
        removeLocationFn && removeLocationFn(location)
    }
    
    function handleSelectClick(e: any) {
        if(e.target.id === 'removeBtn') return
        onClick && onClick();
    }

    return (
        <div className={styles.card} onClick={handleSelectClick}>
            {removeLocationFn && <div className={styles.removeBtn} onClick={handleRemoveClick} id="removeBtn" >&times;</div>}
            {!weather && (<h2>{location.name}</h2>)}
            {weather && (
                <>
                    <Image src={ICON_URL.replace('{ID}', weather.weather[0].icon)} width={35} height={35} alt="forecast" />
                    <h2> {formatTemperatureToString(weather.main.temp)} </h2>
                    <h3> {location.name}</h3>
                </>
            )}
        </div>
    )

}


export default LocationCard;