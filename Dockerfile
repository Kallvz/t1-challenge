FROM node:12-alpine AS build

WORKDIR /app

COPY . .

RUN yarn

ENV NODE_ENV=production

RUN yarn build

EXPOSE 3000

CMD ["yarn", "start"]
