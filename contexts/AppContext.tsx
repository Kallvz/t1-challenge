import React, { useEffect, useState } from "react";
import AlertBox from "../components/AlertBox";
import { DEFAULT_ALERT_TIMEOUT } from "../configs";
import usePosition from "../hooks/usePosition";

//Context Interface
export interface IAppContext {
    selectedLocation?: OptionalGeolocationPosition;
    setSelectedLocation?: (location: OptionalGeolocationPosition) => void;
    alerts: IAlert[];
    showAlert: (alert: IAlert) => void;
}

//Create App Context
export const AppContext = React.createContext<IAppContext | null>(null)

//Context Provider
const AppContextProvider: React.FC<{ children: React.ReactNode }> = ({ children }) => {
    
    //Position/locations Data
    const { position, updatePosition } = usePosition();
    const [selectedLocation, setSelectedLocation] = useState<OptionalGeolocationPosition>();

    //Alerts data
    const [alerts, setAlerts] = useState<IAlert[]>([]);
    
    //set current position on user's position update
    useEffect(() => {
        if (!position) return updatePosition();
        setSelectedLocation(position)
    }, [position])

    //setup alert to show
    function showAlert(alert: IAlert) {
        setAlerts(prevAlerts => [...prevAlerts, alert]);
        //remove alert after timeout
        setTimeout(() => setAlerts(prevAlerts => {
            prevAlerts.shift();
            return [...prevAlerts]
        }), DEFAULT_ALERT_TIMEOUT)
    }

    const contextData: IAppContext = {
        selectedLocation: selectedLocation,
        setSelectedLocation: setSelectedLocation,
        alerts: alerts,
        showAlert: showAlert
    }

    return (
        <AppContext.Provider value={contextData} >
            <AlertBox />
            {children}
        </AppContext.Provider>
    )
} 

//Hook to Context data
export function useAppContextData(): IAppContext {

    const appContext = React.useContext(AppContext);

    if (!appContext) {
        throw new Error('useAppContextData must be used within an AppContextProvider.');
    }

    return appContext;
}

export default AppContextProvider;