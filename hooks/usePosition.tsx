import { useEffect, useState } from "react";
import { DEFAULT_POSITION, POSITION_OPTIONS } from "../configs";

function usePosition() {

    const [position, setPosition] = useState<GeolocationPosition>()

    useEffect(updatePosition, []);

    function updatePosition() {

        navigator.geolocation.getCurrentPosition((pos: GeolocationPosition) => {

            setPosition(pos);

        }, (error) => {
            
            const { PERMISSION_DENIED, POSITION_UNAVAILABLE, TIMEOUT, code, message } = error;
            console.error(error);
            console.error('Setting Leiria as a fallback location');
            setPosition(DEFAULT_POSITION);

        }, POSITION_OPTIONS);
    }

    return {
        position,
        updatePosition
    }

}

export default usePosition;