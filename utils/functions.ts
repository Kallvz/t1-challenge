//Momoize expensive results and keep them in mem cache
export function CacheWrapper (callback: Function) {
  
    const cache = new Map();
  
    return (...args: any[]) => {
        const selector = JSON.stringify(args);
  
        if (cache.has(selector)) return cache.get(selector);
        
        const value = callback(...args);
        cache.set(selector, value);
  
        return value;
    };
};
