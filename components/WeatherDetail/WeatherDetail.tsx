
import { useEffect, useState } from "react";
import styles from "./styles.module.css"
import WeatherService from "../../service/WeatherService";
import Image from "next/image";
import { ICON_URL } from "../../configs";
import { formatTemperatureToString } from "../../utils/formaters";
import ForecastCard from "../ForecastCard";
import { useAppContextData } from "../../contexts/AppContext";

export interface IWeatherDetailProps {
    // location?: GeolocationPosition | OptionalGeolocationPosition;
    // data?: IWeatherForecastData
}

const WeatherDetail: React.FC<IWeatherDetailProps> = () => {

    const { selectedLocation } = useAppContextData();
    const [weatherData, setWeatherData] = useState<IWeatherForecastData>() ;

    useEffect(() => {

        if(!selectedLocation) return

        //Fetch weather data
        WeatherService.getWeatherAndForecast(selectedLocation).then(data => {
            setWeatherData(data);
        });

    },[selectedLocation]);

    if (!weatherData) {
        return (
            <div className={styles.container} >
                <h1 className={styles.loading}> Loading... </h1>
            </div>  
        )
    }

    const { main, description, icon} = weatherData.current.weather[0];

    return (
        <div className={styles.container} >
            
            <div className={styles.currendDayBox} >
                <div className={styles.weather}>
                    <div className={styles.main}>

                        <span className={`column ${styles.iconBox}`} >
                            <Image src={ICON_URL.replace('{ID}', icon)} width={270} height={270} alt="forecast"/>
                        </span>
                        <span className="column" >
                            {formatTemperatureToString(weatherData.current.temp)}
                            <span className={styles.description}>
                              {main}
                            </span>
                        </span>
                        <span className={styles.maxMin}>
                            <p className="no-margin">  &uarr; {formatTemperatureToString(weatherData.daily[0].temp.max)} </p>
                            <p className="no-margin">  &darr; {formatTemperatureToString(weatherData.daily[0].temp.min)} </p>
                        </span>
                    </div>
                </div>
                
                <div className={styles.location}>
                    {weatherData.location && <h2>{weatherData.location.name}</h2> }
                    <p>{description}</p>
                </div>
            </div>

            <div className={styles.nextDaysBox} >
                {weatherData.daily.map((day, i) => i> 0 &&(
                    <ForecastCard forecast={day} key={i} />
                )) }
            </div>

        </div>
    )

}


export default WeatherDetail;